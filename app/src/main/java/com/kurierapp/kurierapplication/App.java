package com.kurierapp.kurierapplication;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kierownik.UzytkowanieUrzadzenia.UzytkowanieUrzadzeniaModel;
import com.kurierapp.kurierapplication.WelcomeActivity.models.KurierModel;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by krzysztofm on 19.02.2018.
 */

public class App extends Application implements SensorEventListener {

    private SensorManager sensorManager;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    private static final String TAG = "App";

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        try {
            mUserId = mFirebaseUser.getUid();
            mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId);
        } catch (Exception e) {
            Log.e("catchUid", "" + e);
        }

        //inicjalizacja sensora
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float value[] = sensorEvent.values;
            float x = value[0];
            float y = value[1];
            float z = value[2];

            float movement_shake = ((x * x + y * y + z * z) / (SensorManager.GRAVITY_EARTH *
                    SensorManager.GRAVITY_EARTH));

            if (movement_shake >= 25) {
                //wykryto wstrzas
                try {
                    mUserId = mFirebaseUser.getUid();
                    mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId);
                    mReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            try {
                                KurierModel kurierModel = dataSnapshot.getValue(KurierModel.class);
                                Log.e(TAG, "onDataChange: " + kurierModel.getTyp());
                                if (kurierModel.getTyp().equals("kurier")) {
                                    SharedPreferences sharedPreferences = getSharedPreferences("kurierProfil", Context.MODE_PRIVATE);
                                    String kierownikKuriera = sharedPreferences.getString("kurierKierownik", "");
                                    Log.e(TAG, "onDataChange, kierownik kuriera: " + kierownikKuriera);
                                    //wysylamy powiadomienie
                                    mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kierownicy").child(kierownikKuriera).child("UzytkowanieUrzadznia");
                                    mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            //data godzina
                                            String archiveTime = DateFormat.getDateTimeInstance().format(new Date());

                                            //pobieram maila kuriera
                                            SharedPreferences sharedPreferences = getSharedPreferences("kurierProfil", Context.MODE_PRIVATE);
                                            String kurierKierownik = sharedPreferences.getString("kurierKierownik", "");
                                            String kurierEmail = sharedPreferences.getString("kurierEmail", "");

                                            Log.e(TAG, "onDataChange,email kuiera: " + kurierEmail);
                                            Log.e(TAG, "onDataChange,email kuiera: " + kurierKierownik);

                                            //ustawienie modelu do wyslania ze potrzasnal telefonem
                                            UzytkowanieUrzadzeniaModel uzytkowanieUrzadzeniaModel = new UzytkowanieUrzadzeniaModel(
                                                    kurierEmail,
                                                    archiveTime
                                            );
                                            //wyslanie rekordu
                                            try {
                                                mReference
                                                        .push()
                                                        .setValue(uzytkowanieUrzadzeniaModel);
                                            } catch (Exception e) {
                                                Log.e("pushing", "exception: " + e);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                            } catch (
                                    NullPointerException e) {
                                Log.e("exception1", "" + e);

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, "onCancelled: databaseerror: " + databaseError);
                        }
                    });
                } catch (Exception e) {
                    Log.e("exception2", "" + e);
                }
            } else {

            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
