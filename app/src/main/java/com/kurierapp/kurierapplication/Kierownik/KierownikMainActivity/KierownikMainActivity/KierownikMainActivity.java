package com.kurierapp.kurierapplication.Kierownik.KierownikMainActivity.KierownikMainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kierownik.KierownikMap.KierownikMapActivity;
import com.kurierapp.kurierapplication.Kierownik.KierownikMojaListaKurierowFragment.KierownikMojaListaKurierowFragment;
import com.kurierapp.kurierapplication.Kierownik.UzytkowanieUrzadzenia.UzytkowanieUrzadzeniaFragment;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktFragment;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;
import com.kurierapp.kurierapplication.WelcomeActivity.models.KurierModel;

import butterknife.ButterKnife;

//aktywnosc po zalogowaniu sie kierownika, pusto poza wysuwanym menu i wylogowaniem w prawym gornym rogu
public class KierownikMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, KierownikMainActivityMVP.View {

    private KierownikMainActivityPresenter presenter;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private String mUserId;
    private DatabaseReference mReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        //obiekt presentera
        presenter = new KierownikMainActivityPresenter(this, this);

        //sprawdz czy uzytkownik jest zalogowany
        checkUserAuth();

        //sprawdzamy czy kureir sie zalogowal czy kierownik, jezeli kierownik to odrazu go wylogowwuje.
        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId);
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    KurierModel kurierModel = dataSnapshot.getValue(KurierModel.class);
                    if (kurierModel.getTyp().equals("kurier")) {
                        presenter.onLogoutButtonClicked();
                        Toast.makeText(KierownikMainActivity.this, "Nie jesteś kierownikiem.", Toast.LENGTH_SHORT).show();
                    }
                } catch (NullPointerException e) {
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //obiekt wysuwanego menu
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        TextView emailKierownika = header.findViewById(R.id.tvKierownikEmail);
        emailKierownika.setText(mFirebaseUser.getEmail());

        //open fragment moja lista
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, KierownikMojaListaKurierowFragment.newInstance());
        fragmentTransaction.commit();
    }

    //sprawdzenie czy uzytkownik jest zalogowany
    @Override
    public void checkUserAuth() {
        presenter.checkUserAuth();
    }

    //przejscie do ekranu logowania
    @Override
    public void loadLoginActivity() {
        startActivity(new Intent(KierownikMainActivity.this, WelcomeActivity.class));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

            return;
        }
        Toast.makeText(this, "Wyloguj się", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            presenter.onLogoutButtonClicked();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment selectedFragment = KurierzyKontaktFragment.newInstance();
        //wysuwane menu
        int id = item.getItemId();

        //przyciski
        if (id == R.id.nav_kurierzy) {
            selectedFragment = KurierzyKontaktFragment.newInstance();
            //startActivity(new Intent(KierownikMainActivity.this, KierownikMapActivity.class));
        } else if (id == R.id.nav_mapa) {
            startActivity(new Intent(KierownikMainActivity.this, KierownikMapActivity.class));
        } else if (id == R.id.nav_moi_kurierzy) {
            selectedFragment = KierownikMojaListaKurierowFragment.newInstance();
        } else if (id == R.id.nav_uzytkownie_urzadzenia) {
            selectedFragment = UzytkowanieUrzadzeniaFragment.newInstance();
        }

        //open fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.replace(R.id.content, selectedFragment);
        fragmentTransaction.commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}