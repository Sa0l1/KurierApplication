package com.kurierapp.kurierapplication.Kierownik.KierownikMainActivity.KierownikMainActivity;

/**
 * Created by staszekalcatraz on 17/01/2018.
 */


public interface KierownikMainActivityMVP {

    interface View{
        void checkUserAuth();

        void loadLoginActivity();
    }

    interface Presenter{
        void checkUserAuth();

        void onLogoutButtonClicked();
    }
}
