package com.kurierapp.kurierapplication.Kierownik.KierownikMainActivity.KierownikMainActivity;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by staszekalcatraz on 17/01/2018.
 */

class KierownikMainActivityPresenter implements KierownikMainActivityMVP.Presenter {

    private Context context;
    private KierownikMainActivityMVP.View view;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUserId;

    public KierownikMainActivityPresenter(Context context, KierownikMainActivityMVP.View view) {
        this.context = context;
        this.view = view;
    }

    //sprawdzam uzytkownik jest zalogowany
    @Override
    public void checkUserAuth() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mUserId = mFirebaseAuth.getCurrentUser().getUid();

        try {
            if (mFirebaseUser != null) {
                mUserId = mFirebaseUser.getEmail();
            }
        } catch (Exception e) {
            Log.e("cannot find mUserId", "");
            view.loadLoginActivity();
        }
        if (mUserId == null) {
            view.loadLoginActivity();
        } else {
            Log.e("mUserId", "" + mUserId);
        }
    }

    //wyloguj
    @Override
    public void onLogoutButtonClicked() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.signOut();
        view.loadLoginActivity();

    }
}
