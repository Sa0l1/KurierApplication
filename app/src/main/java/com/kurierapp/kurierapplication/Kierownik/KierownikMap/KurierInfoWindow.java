package com.kurierapp.kurierapplication.Kierownik.KierownikMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.kurierapp.kurierapplication.R;

/**
 * Created by staszekalcatraz on 13/01/2018.
 */

//okienko z informacjami po kliknieciu na punkt
public class KurierInfoWindow implements GoogleMap.InfoWindowAdapter {

    private View mView;


    public KurierInfoWindow(Context context) {
        mView = LayoutInflater.from(context)
                .inflate(R.layout.kurier_info_window, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        TextView tvKurierInfo = mView.findViewById(R.id.tvKurierInfo);
        tvKurierInfo.setText(marker.getTitle());

        TextView tvKurierSnippet = mView.findViewById(R.id.tvKurierSnippet);
        tvKurierSnippet.setText(marker.getSnippet());

        return mView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
