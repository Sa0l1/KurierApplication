package com.kurierapp.kurierapplication.Kierownik.KierownikMojaListaKurierowFragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierDetailsActivity;
import com.kurierapp.kurierapplication.R;

import java.util.List;


/**
 * Created by krzysztofm on 02.02.2018.
 */

public class KierownikMojaListaKurierowDataAdapter extends RecyclerView.Adapter<KierownikMojaListaKurierowDataAdapter.ViewHolder> {

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    KierownikMojaListaKurierowDataAdapter adapter;

    private Context context;
    private List<ListaKurierowModel> kurierModelList;

    public KierownikMojaListaKurierowDataAdapter(Context context, List<ListaKurierowModel> kurierModelList) {
        this.context = context;
        this.kurierModelList = kurierModelList;
    }

    @Override
    public KierownikMojaListaKurierowDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.kurier_kontakty_row, parent, false);
        KierownikMojaListaKurierowDataAdapter.ViewHolder viewHolder = new KierownikMojaListaKurierowDataAdapter.ViewHolder(view);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(KierownikMojaListaKurierowDataAdapter.ViewHolder holder, final int position) {
        final ListaKurierowModel kurierModel = kurierModelList.get(position);
        holder.tvKurierEmail.setText(kurierModel.getEmail());
        holder.tvKurierTelefon.setText(kurierModel.getPhoneNumber());

        final String kurierUuid = kurierModel.getUuid();
        Log.e("adapter", "" + kurierUuid);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), KurierDetailsActivity.class);
                intent.putExtra("id", "1");
                intent.putExtra("kurierEmail", "" + kurierModel.getEmail());
                intent.putExtra("kurierPhone", "" + kurierModel.getPhoneNumber());
                intent.putExtra("kurierUuid", "" + kurierUuid);
                /*intent.putExtra("position", "" + position);
                Log.e("position", "position: " + position);*/
                view.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return kurierModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvKurierEmail, tvKurierTelefon;

        public ViewHolder(View itemView) {
            super(itemView);

            tvKurierEmail = itemView.findViewById(R.id.tvKurierEmail);
            tvKurierTelefon = itemView.findViewById(R.id.tvKurierTelefon);

        }
    }
}