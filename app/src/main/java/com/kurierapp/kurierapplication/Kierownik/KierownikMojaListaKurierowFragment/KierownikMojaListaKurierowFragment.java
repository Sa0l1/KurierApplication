package com.kurierapp.kurierapplication.Kierownik.KierownikMojaListaKurierowFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierDetailsActivity;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktDataAdapter;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktModel;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;

import java.util.ArrayList;
import java.util.List;

public class KierownikMojaListaKurierowFragment extends Fragment {

    private List<ListaKurierowModel> kurierModelList;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    public KierownikMojaListaKurierowFragment() {
        // Required empty public constructor
    }

    public static KierownikMojaListaKurierowFragment newInstance() {
        KierownikMojaListaKurierowFragment fragment = new KierownikMojaListaKurierowFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kierownik_moja_lista_kurierow, container, false);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy");

        //jezeli nie zalogowany, wyloguj
        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(getActivity(), WelcomeActivity.class));
        }

        //ladujemy liste kurierow kierownika
        final ListView lvListaKurierow = view.findViewById(R.id.lvListaKurierow);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1);
        lvListaKurierow.setAdapter(adapter);

        mReference.child("Kierownicy").child(mUserId).child("ListaMoichKurierow").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.add((String) dataSnapshot.child("email").getValue());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.remove((String) dataSnapshot.child("email").getValue());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lvListaKurierow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                AlertDialog.Builder opcjeDialog = new AlertDialog.Builder(getActivity());
                opcjeDialog.setTitle("Wybierz");

                opcjeDialog.setPositiveButton("Wyswietl", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        mFirebaseDatabase.getReference("Uzytkownicy").child("Kierownicy").child(mUserId).child("ListaMoichKurierow")
                                .orderByChild("email")
                                .equalTo((String) lvListaKurierow.getItemAtPosition(position))
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Intent intent = new Intent(getActivity(), KurierDetailsActivity.class);
                                        intent.putExtra("id", "1");
                                        intent.putExtra("kurierEmail", "" + dataSnapshot.getChildren().iterator().next().getValue(ListaKurierowModel.class).getEmail());
                                        intent.putExtra("kurierPhone", "" + dataSnapshot.getChildren().iterator().next().getValue(ListaKurierowModel.class).getPhoneNumber());
                                        intent.putExtra("kurierUuid", "" + dataSnapshot.getChildren().iterator().next().getValue(ListaKurierowModel.class).getUuid());
                                        getActivity().startActivity(intent);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("error", "" + databaseError.getMessage());
                                    }
                                });
                    }
                }).setNegativeButton("Usuń", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mReference.child("Kierownicy").child(mUserId).child("ListaMoichKurierow")
                                .orderByChild("email")
                                .equalTo((String) lvListaKurierow.getItemAtPosition(position))
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot dataSnapshot) {
                                        AlertDialog.Builder usunDialog = new AlertDialog.Builder(getActivity());
                                        usunDialog.setTitle("Czy chcesz usunąć kuriera ze swojej listy?");
                                        usunDialog.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (dataSnapshot.hasChildren()) {
                                                    DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                                                    firstChild.getRef().removeValue();
                                                    Log.e("firstchild", "" + firstChild);
                                                    Toast.makeText(getActivity(), "Usunięto", Toast.LENGTH_SHORT).show();

                                                    dialogInterface.dismiss();
                                                }
                                            }
                                        }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                        usunDialog.create().show();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("onCancelled databaseErr", "" + databaseError);
                                    }

                                });

                    }
                });
                opcjeDialog.create().show();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
