package com.kurierapp.kurierapplication.Kierownik.KierownikMojaListaKurierowFragment;

/**
 * Created by krzysztofm on 02.02.2018.
 */

public class ListaKurierowModel {
    String email;
    String phoneNumber;
    String uuid;

    public ListaKurierowModel() {
    }

    public ListaKurierowModel(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
