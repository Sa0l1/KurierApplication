package com.kurierapp.kurierapplication.Kierownik.UzytkowanieUrzadzenia;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktDataAdapter;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktModel;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UzytkowanieUrzadzeniaFragment extends Fragment {

    private List<UzytkowanieUrzadzeniaModel> uzytkowanieUrzadzeniaModelList;
    private UzytkownicyUrzadzeniaDataAdapter uzytkownicyUrzadzeniaDataAdapter;

    private RecyclerView rvListaWstrzasowUrzadzen;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    public UzytkowanieUrzadzeniaFragment() {
        // Required empty public constructor
    }

    public static UzytkowanieUrzadzeniaFragment newInstance() {
        UzytkowanieUrzadzeniaFragment fragment = new UzytkowanieUrzadzeniaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_uzytkowanie_urzadzenia, container, false);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy");

        //jezeli nie zalogowany, wyloguj
        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(getActivity(), WelcomeActivity.class));
        }

        //lista
        rvListaWstrzasowUrzadzen = view.findViewById(R.id.rvListaWstrzasowUrzadzen);
        rvListaWstrzasowUrzadzen.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvListaWstrzasowUrzadzen.setLayoutManager(llm);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        loadRecyclerViewData();

        return view;
    }

    private void loadRecyclerViewData() {
        mReference.child("Kierownicy").child(mUserId).child("UzytkowanieUrzadznia").orderByChild("data").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                uzytkowanieUrzadzeniaModelList = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    UzytkowanieUrzadzeniaModel value = dataSnapshot1.getValue(UzytkowanieUrzadzeniaModel.class);
                    String valueKey = dataSnapshot1.getKey();
                    UzytkowanieUrzadzeniaModel uzytkowanieUrzadzeniaModel = new UzytkowanieUrzadzeniaModel();

                    Log.e("valueKey", "" + valueKey);
                    uzytkowanieUrzadzeniaModel.setKurier(value.getKurier());
                    uzytkowanieUrzadzeniaModel.setData(value.getData());

                    uzytkowanieUrzadzeniaModelList.add(uzytkowanieUrzadzeniaModel);

                    uzytkownicyUrzadzeniaDataAdapter = new UzytkownicyUrzadzeniaDataAdapter(getActivity(), uzytkowanieUrzadzeniaModelList);
                    rvListaWstrzasowUrzadzen.setAdapter(uzytkownicyUrzadzeniaDataAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", "" + databaseError.getMessage());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
