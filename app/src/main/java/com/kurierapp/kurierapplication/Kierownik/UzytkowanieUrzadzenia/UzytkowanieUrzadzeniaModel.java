package com.kurierapp.kurierapplication.Kierownik.UzytkowanieUrzadzenia;

/**
 * Created by krzysztofm on 20.02.2018.
 */

public class UzytkowanieUrzadzeniaModel {
    private String kurier;
    private String data;

    public UzytkowanieUrzadzeniaModel() {
    }

    public UzytkowanieUrzadzeniaModel(String kurier, String data) {
        this.kurier = kurier;
        this.data = data;
    }

    public String getKurier() {
        return kurier;
    }

    public void setKurier(String kurier) {
        this.kurier = kurier;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
