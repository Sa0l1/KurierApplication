package com.kurierapp.kurierapplication.Kierownik.UzytkowanieUrzadzenia;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kurierapp.kurierapplication.R;

import java.util.List;

/**
 * Created by krzysztofm on 20.02.2018.
 */

public class UzytkownicyUrzadzeniaDataAdapter extends RecyclerView.Adapter<UzytkownicyUrzadzeniaDataAdapter.ViewHolder> {

    private Context context;
    private List<UzytkowanieUrzadzeniaModel> uzytkowanieUrzadzeniaModelList;

    public UzytkownicyUrzadzeniaDataAdapter(Context context, List<UzytkowanieUrzadzeniaModel> uzytkowanieUrzadzeniaModelList) {
        this.context = context;
        this.uzytkowanieUrzadzeniaModelList = uzytkowanieUrzadzeniaModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.uzytkownicy_urzadzenia_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UzytkownicyUrzadzeniaDataAdapter.ViewHolder holder, int position) {
        UzytkowanieUrzadzeniaModel uzytkowanieUrzadzeniaModel = uzytkowanieUrzadzeniaModelList.get(position);
        Log.e("getKurier()", "" + uzytkowanieUrzadzeniaModel.getKurier());
        Log.e("tvData()", "" + uzytkowanieUrzadzeniaModel.getData());

        holder.tvKurierEmail.setText(uzytkowanieUrzadzeniaModel.getKurier());
        holder.tvData.setText(uzytkowanieUrzadzeniaModel.getData());
    }

    @Override
    public int getItemCount() {
        return uzytkowanieUrzadzeniaModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvKurierEmail, tvData;

        public ViewHolder(View itemView) {
            super(itemView);

            tvKurierEmail = itemView.findViewById(R.id.tvKurierEmail);
            tvData = itemView.findViewById(R.id.tvData);
        }
    }
}
