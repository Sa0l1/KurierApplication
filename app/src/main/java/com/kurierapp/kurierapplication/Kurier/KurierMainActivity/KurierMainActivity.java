package com.kurierapp.kurierapplication.Kurier.KurierMainActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurier.KurierMapActivity.KurierMapActivity;
import com.kurierapp.kurierapplication.Kurier.KurierMojProfilFragment.KurierMojProfilFragment;
import com.kurierapp.kurierapplication.Kurier.KurierzyMojegoKierownika.KurierzyMojegoKierownikaFragment;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktFragment;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;
import com.kurierapp.kurierapplication.WelcomeActivity.models.KurierModel;

public class KurierMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, KurierMainActivityMVP.View {

    private KurierMainActivityPresenter presenter;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private String mUserId;
    private DatabaseReference mReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kurier_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //obiekt presentera
        presenter = new KurierMainActivityPresenter(this, this);

        //sprawdz czy uzytkownik jest zalogowany
        presenter.checkUserAuth();

        //sprawdzamy czy kureir sie zalogowal czy kierownik, jezeli kierownik to odrazu go wylogowwuje.
        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kierownicy").child(mUserId);
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    KurierModel kurierModel = dataSnapshot.getValue(KurierModel.class);
                    if (kurierModel.getTyp().equals("kierownik")) {
                        presenter.onLogoutButtonClicked();
                        Toast.makeText(KurierMainActivity.this, "Nie jesteś kurierem.", Toast.LENGTH_SHORT).show();
                    }
                } catch (NullPointerException e) {
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //zalogowany kurier
        SharedPreferences sharedPreferencesUserLogin = getSharedPreferences("ktoZalogowany", Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPrefEditor = sharedPreferencesUserLogin.edit();
        sharedPrefEditor.putString("ktoZalogowany", "kurier");
        sharedPrefEditor.apply();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        TextView emailKuriera = header.findViewById(R.id.tvKurierEmail);
        emailKuriera.setText(mFirebaseUser.getEmail());

        //open fragment moj profil
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, KurierMojProfilFragment.newInstance());
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

            return;
        }

        Toast.makeText(this, "Wyloguj się", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.kurier_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            //sprawdzam czy kurier czy kierownik
            SharedPreferences sharedPreferencesUserLogin = getSharedPreferences("ktoZalogowany", Context.MODE_PRIVATE);
            SharedPreferences.Editor sharedPrefEditor = sharedPreferencesUserLogin.edit();
            sharedPrefEditor.putString("ktoZalogowany", "");
            sharedPrefEditor.apply();
            presenter.onLogoutButtonClicked();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment selectedFragment = KurierMojProfilFragment.newInstance();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_mapa_kuriera) {
            startActivity(new Intent(KurierMainActivity.this, KurierMapActivity.class));
        } else if (id == R.id.nav_wszyscy_kurierzy) {
            selectedFragment = KurierzyMojegoKierownikaFragment.newInstance();
        } else if (id == R.id.nav_moj_profil) {
            selectedFragment = KurierMojProfilFragment.newInstance();
        }

        //open fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.replace(R.id.content, selectedFragment);
        fragmentTransaction.commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void loadLoginActivity() {
        startActivity(new Intent(KurierMainActivity.this, WelcomeActivity.class));
    }
}
