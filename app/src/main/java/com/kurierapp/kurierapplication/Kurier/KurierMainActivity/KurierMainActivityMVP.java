package com.kurierapp.kurierapplication.Kurier.KurierMainActivity;

/**
 * Created by staszekalcatraz on 18/01/2018.
 */

public interface KurierMainActivityMVP {
    interface View {
        void loadLoginActivity();
    }

    interface Presenter {
        void checkUserAuth();

        void onLogoutButtonClicked();
    }
}
