package com.kurierapp.kurierapplication.Kurier.KurierMainActivity;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by staszekalcatraz on 18/01/2018.
 */

class KurierMainActivityPresenter implements KurierMainActivityMVP.Presenter {

    private KurierMainActivityMVP.View view;

    private Context context;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUserId;

    public KurierMainActivityPresenter(KurierMainActivityMVP.View view, Context context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void checkUserAuth() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mUserId = mFirebaseAuth.getCurrentUser().getUid();

        try {
            if (mFirebaseUser != null) {
                mUserId = mFirebaseUser.getEmail();
            }
        } catch (Exception e) {
            Log.e("cannot find mUserId", "");
            view.loadLoginActivity();
        }
        if (mUserId == null) {
            view.loadLoginActivity();
        } else {
            Log.e("mUserId", "" + mUserId);
        }
    }

    @Override
    public void onLogoutButtonClicked() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.signOut();
        view.loadLoginActivity();
    }
}
