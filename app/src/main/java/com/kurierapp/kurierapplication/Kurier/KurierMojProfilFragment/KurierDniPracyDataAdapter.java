package com.kurierapp.kurierapplication.Kurier.KurierMojProfilFragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierDniPracyModel;
import com.kurierapp.kurierapplication.R;

import java.util.List;

/**
 * Created by staszekalcatraz on 13/02/2018.
 */

public class KurierDniPracyDataAdapter extends RecyclerView.Adapter<KurierDniPracyDataAdapter.ViewHolder> {

    private Context context;
    private List<KurierDniPracyModel> kurierDniPracyModelList;

    public KurierDniPracyDataAdapter(Context context, List<KurierDniPracyModel> kurierDniPracyModelList) {
        this.context = context;
        this.kurierDniPracyModelList = kurierDniPracyModelList;
    }

    @Override
    public KurierDniPracyDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.kurier_dni_pracy_row, parent, false);
        KurierDniPracyDataAdapter.ViewHolder viewHolder = new KurierDniPracyDataAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(KurierDniPracyDataAdapter.ViewHolder holder, int position) {
        KurierDniPracyModel kurierDniPracyModel = kurierDniPracyModelList.get(position);
        holder.tvDzien.setText(kurierDniPracyModel.getDzien());
        holder.tvGodzinaStart.setText(kurierDniPracyModel.getGodzinaStart());
        holder.tvGodzinaKoniec.setText(kurierDniPracyModel.getGodzinaKoniec());
    }

    @Override
    public int getItemCount() {
        return kurierDniPracyModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDzien;
        private TextView tvGodzinaStart;
        private TextView tvGodzinaKoniec;

        public ViewHolder(View itemView) {
            super(itemView);

            tvDzien = itemView.findViewById(R.id.tvDzien);
            tvGodzinaStart = itemView.findViewById(R.id.tvGodzinaStart);
            tvGodzinaKoniec = itemView.findViewById(R.id.tvGodzinaKoniec);
        }
    }
}
