package com.kurierapp.kurierapplication.Kurier.KurierMojProfilFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierDniPracyModel;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierGrafikModel;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierzyScheduleDataAdapter;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktModel;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;
import com.kurierapp.kurierapplication.WelcomeActivity.models.KurierModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class KurierMojProfilFragment extends Fragment {

    private static final String TAG = "KurierMojProfilFragment";

    @BindView(R.id.tvZalogowany)
    TextView tvZalogowany;

    @BindView(R.id.tvKurierPhone)
    TextView tvKurierPhone;

    @BindView(R.id.rvMojaListaZadan)
    RecyclerView rvMojaListaZadan;

    @BindView(R.id.rvMojeDniPracy)
    RecyclerView rvMojeDniPracy;

    @BindView(R.id.rvPowiadomieniaKuriera)
    RecyclerView rvPowiadomieniaKuriera;

    @BindView(R.id.vZadania)
    View vZadania;

    @BindView(R.id.vPowiadomienia)
    View vPowiadomienia;

    @BindView(R.id.vDniPracy)
    View vDniPracy;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    private List<KurierGrafikModel> kurierScheduleList;
    private List<KurierDniPracyModel> kurierDniPracyModelList;
    private List<KurierzyKontaktModel> kurierPowiadomieniaModelList;
    private Unbinder unbind;

    public KurierMojProfilFragment() {
        // Required empty public constructor
    }

    public static KurierMojProfilFragment newInstance() {
        KurierMojProfilFragment fragment = new KurierMojProfilFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kurier_moj_profil, container, false);
        unbind = ButterKnife.bind(this, view);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId);
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                KurierModel kurierModel = dataSnapshot.getValue(KurierModel.class);
                try {
                    tvZalogowany.setText("Zalogowany jako: " + kurierModel.getEmail());
                    tvKurierPhone.setText("Tel: " + kurierModel.getPhoneNumber());

                    Log.e(TAG, "onDataChange: " + kurierModel.getEmail());

                    //wpisujemy maila naszego kuriera do pozniejszego uzytku np przy wysylaniu maila na server przy potrzasnieciu
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("kurierProfil", Context.MODE_PRIVATE);
                    SharedPreferences.Editor sharedPrefEditorKurier = sharedPreferences.edit();
                    sharedPrefEditorKurier.putString("kurierEmail", "" + kurierModel.getEmail());
                    sharedPrefEditorKurier.putString("kurierKierownik", "" + kurierModel.getPassword());
                    Log.e("TAG", "onDataChange: " + kurierModel.getEmail());
                    sharedPrefEditorKurier.apply();

                } catch (Exception e) {
                    Log.e("TAG", "onDataChange: " + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //jezeli nie zalogowany, wyloguj
        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(getActivity(), WelcomeActivity.class));
        }

        //lista zadan
        rvMojaListaZadan.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvMojaListaZadan.setLayoutManager(llm);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        //lista dni pracy
        rvMojeDniPracy.setHasFixedSize(true);
        LinearLayoutManager llm2 = new LinearLayoutManager(getActivity());
        rvMojeDniPracy.setLayoutManager(llm2);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);

        //lista powiadomien
        rvPowiadomieniaKuriera.setHasFixedSize(true);
        LinearLayoutManager llm3 = new LinearLayoutManager(getActivity());
        rvPowiadomieniaKuriera.setLayoutManager(llm3);
        llm3.setOrientation(LinearLayoutManager.VERTICAL);

        loadRecyclerViewDataListaZadan();
        loadRecyclerViewDataListaDniPracy();
        loadRecyclerViewPowiadomienia();

        return view;
    }

    @OnClick(R.id.textZadania)
    public void loadRecyclerViewDataListaZadan() {
        rvMojaListaZadan.setVisibility(View.VISIBLE);
        rvPowiadomieniaKuriera.setVisibility(View.GONE);
        rvMojeDniPracy.setVisibility(View.GONE);
        vZadania.setVisibility(View.VISIBLE);
        vPowiadomienia.setVisibility(View.GONE);
        vDniPracy.setVisibility(View.GONE);

        rvMojaListaZadan.invalidate();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId).child("Grafik");
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                kurierScheduleList = new ArrayList<>();
                try {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        KurierGrafikModel value = dataSnapshot1.getValue(KurierGrafikModel.class);
                        KurierGrafikModel kurierGrafikModel = new KurierGrafikModel();

                        kurierGrafikModel.setPriorytet(value.getPriorytet());
                        kurierGrafikModel.setNazwa(value.getNazwa());
                        kurierGrafikModel.setStatus(value.getStatus());
                        kurierGrafikModel.setOpis(value.getOpis());
                        kurierGrafikModel.setKey(dataSnapshot1.getKey());
                        Log.e(TAG, "onDataChange: key: " + dataSnapshot1.getKey());
                        kurierGrafikModel.setDzien(value.getDzien());
                        kurierGrafikModel.setGodzina(value.getGodzina());
                        kurierGrafikModel.setMiasto(value.getMiasto());
                        kurierGrafikModel.setUlica(value.getUlica());
                        kurierGrafikModel.setUuid(mUserId);

                        kurierScheduleList.add(kurierGrafikModel);

                        KurierzyScheduleDataAdapter kurierzyScheduleDataAdapter = new KurierzyScheduleDataAdapter(getActivity(), kurierScheduleList);
                        rvMojaListaZadan.setAdapter(kurierzyScheduleDataAdapter);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onDataChange: exception " + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", "" + databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.textDniPracy)
    public void loadRecyclerViewDataListaDniPracy() {
        rvMojaListaZadan.setVisibility(View.GONE);
        rvPowiadomieniaKuriera.setVisibility(View.GONE);
        rvMojeDniPracy.setVisibility(View.VISIBLE);

        vZadania.setVisibility(View.GONE);
        vPowiadomienia.setVisibility(View.GONE);
        vDniPracy.setVisibility(View.VISIBLE);

        rvMojaListaZadan.invalidate();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId).child("DniPracy");
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                kurierDniPracyModelList = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    KurierDniPracyModel value = dataSnapshot1.getValue(KurierDniPracyModel.class);
                    KurierDniPracyModel kurierDniPracyModel = new KurierDniPracyModel();

                    kurierDniPracyModel.setDzien(value.getDzien());
                    kurierDniPracyModel.setGodzinaStart(value.getGodzinaStart());
                    kurierDniPracyModel.setGodzinaKoniec(value.getGodzinaKoniec());

                    kurierDniPracyModelList.add(kurierDniPracyModel);

                    KurierDniPracyDataAdapter kurierDniPracyDataAdapter = new KurierDniPracyDataAdapter(getActivity(), kurierDniPracyModelList);
                    rvMojeDniPracy.setAdapter(kurierDniPracyDataAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", "" + databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.textPowiadomienia)
    public void loadRecyclerViewPowiadomienia() {
        rvMojaListaZadan.setVisibility(View.GONE);
        rvPowiadomieniaKuriera.setVisibility(View.VISIBLE);
        rvMojeDniPracy.setVisibility(View.GONE);

        vZadania.setVisibility(View.GONE);
        vPowiadomienia.setVisibility(View.VISIBLE);
        vDniPracy.setVisibility(View.GONE);

        rvPowiadomieniaKuriera.invalidate();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId).child("Powiadomienia");
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                kurierPowiadomieniaModelList = new ArrayList<>();
                try {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        KurierzyKontaktModel value = dataSnapshot1.getValue(KurierzyKontaktModel.class);
                        KurierzyKontaktModel kurierModel = new KurierzyKontaktModel();
                        Log.e(TAG, "onDataChange: powiadomienie key: " + dataSnapshot1.getKey());

                        //uuid kierownika
                        kurierModel.setPassword(value.getPassword());
                        kurierModel.setTyp(value.getTyp());
                        kurierModel.setEmail(value.getEmail());
                        kurierModel.setPhoneNumber(value.getPhoneNumber());
                        kurierModel.setUuid(value.getUuid());

                        kurierPowiadomieniaModelList.add(kurierModel);

                        KurierPowiadomieniaDataAdapter kurierPowiadomieniaDataAdapter = new KurierPowiadomieniaDataAdapter(getActivity(), kurierPowiadomieniaModelList);
                        rvPowiadomieniaKuriera.setAdapter(kurierPowiadomieniaDataAdapter);
                    }
                } catch (Exception e) {
                    Log.e("AG", "onDataChange: " + e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", "" + databaseError.getMessage());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbind.unbind();
    }
}
