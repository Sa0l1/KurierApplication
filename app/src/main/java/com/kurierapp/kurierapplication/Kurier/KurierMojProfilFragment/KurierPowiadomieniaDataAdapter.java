package com.kurierapp.kurierapplication.Kurier.KurierMojProfilFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktModel;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.models.KurierModel;

import java.util.List;

/**
 * Created by krzysztofm on 15.02.2018.
 */

public class KurierPowiadomieniaDataAdapter extends RecyclerView.Adapter<KurierPowiadomieniaDataAdapter.ViewHolder> {
    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private DatabaseReference mReference2;

    private String mUserId;
    private Context context;
    private List<KurierzyKontaktModel> kurierModelList;

    public KurierPowiadomieniaDataAdapter(Context context, List<KurierzyKontaktModel> kurierModelList) {
        this.context = context;
        this.kurierModelList = kurierModelList;
    }

    @Override
    public KurierPowiadomieniaDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.kurier_powiadomienia_row, parent, false);
        KurierPowiadomieniaDataAdapter.ViewHolder viewHolder = new KurierPowiadomieniaDataAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(KurierPowiadomieniaDataAdapter.ViewHolder holder, int position) {
        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy");
        mReference2 = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId).child("Powiadomienia");

        final KurierzyKontaktModel kurierModel = kurierModelList.get(position);

        holder.tvPowiadomienie.setText(kurierModel.getTyp() + " chce abyś był jego kurierem.");
        String phoneNumber = kurierModel.getPhoneNumber();
        String email = kurierModel.getEmail();
        kurierModel.setPhoneNumber(phoneNumber);
        kurierModel.setEmail(email);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder powiadomienieDialog = new AlertDialog.Builder(context);
                powiadomienieDialog.setTitle(R.string.potwierdz);

                powiadomienieDialog.setPositiveButton("Zatwierdź", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            final String kieroUuid = kurierModel.getPassword();

                            //dodanie kuriera do kierownika
                            mReference.child("Kierownicy")
                                    .child(kieroUuid)
                                    .child("ListaMoichKurierow")
                                    .push()
                                    .setValue(kurierModel);

                            //usuniecie powiadomien
                            mReference2.removeValue();

                            mReference.child("Kurierzy").child(mUserId);
                            mReference.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    try {
                                        KurierModel kurierModel = dataSnapshot.getValue(KurierModel.class);
                                        kurierModel.setPassword(kieroUuid);
                                        try {
                                            //ustawienie id kierownika
                                            mReference.child("Kurierzy")
                                                    .child(mUserId)
                                                    .child("password")
                                                    .setValue(kieroUuid);

                                        } catch (Exception e) {
                                        }
                                    } catch (NullPointerException e) {
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            Toast.makeText(context, "Dodano.", Toast.LENGTH_SHORT).show();
                            //view.openMealsFragment();
                        } catch (Exception e) {
                            Toast.makeText(context, "Błąd", Toast.LENGTH_SHORT).show();
                            Log.e("error pushing item", "" + e.toString());
                        }
                    }
                }).setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        AlertDialog.Builder powiadomienieDialog = new AlertDialog.Builder(context);
                        powiadomienieDialog.setTitle("Usunąć powiadomienie?");

                        powiadomienieDialog.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mReference2.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        try {
                                            KurierzyKontaktModel kurierzyKontaktModel = dataSnapshot.getValue(KurierzyKontaktModel.class);
                                            try {
                                                final String kieroUuid = kurierzyKontaktModel.getPassword();
                                                //mReference.child("Powiadomienia").setValue(null);

                                                if (dataSnapshot.hasChildren()) {
                                                    DataSnapshot data = dataSnapshot.getChildren().iterator().next();//getRef();

                                                    data.getRef().removeValue();
                                                }

                                            } catch (Exception e) {
                                            }
                                        } catch (NullPointerException e) {
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                            }
                        }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        powiadomienieDialog.create().show();
                    }
                });
                powiadomienieDialog.create().show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return kurierModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPowiadomienie;

        public ViewHolder(View itemView) {
            super(itemView);

            tvPowiadomienie = itemView.findViewById(R.id.tvPowiadomienieRow);
        }
    }
}
