package com.kurierapp.kurierapplication.Kurier.KurierzyMojegoKierownika;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktDataAdapter;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktModel;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;

import java.util.ArrayList;
import java.util.List;


public class KurierzyMojegoKierownikaFragment extends Fragment {

    private RecyclerView rvListaKurierowMojegoKierownika;
    private List<KurierzyKontaktModel> kurierModelList;
    private KurierzyKontaktDataAdapter kurierzyKontaktDataAdapter;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    public KurierzyMojegoKierownikaFragment() {
        // Required empty public constructor
    }

    public static KurierzyMojegoKierownikaFragment newInstance() {
        KurierzyMojegoKierownikaFragment fragment = new KurierzyMojegoKierownikaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kurierzy_mojego_kierownika, container, false);

        //lista
        rvListaKurierowMojegoKierownika = view.findViewById(R.id.rvListaKurierowMojegoKierownika);
        rvListaKurierowMojegoKierownika.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvListaKurierowMojegoKierownika.setLayoutManager(llm);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserId = mFirebaseUser.getUid();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(mUserId);
        Log.e("mReference", "" + mReference.toString());
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    //pobranie mojego kierownika
                    KurierzyKontaktModel kurierModel = dataSnapshot.getValue(KurierzyKontaktModel.class);
                    Log.e("getPassword", "" + kurierModel.getPassword());
                    String mojKierownik = kurierModel.getPassword();
                    Log.e("mojkierownik", "" + mojKierownik);

                    loadRecyclerViewData(mojKierownik);
                } catch (NullPointerException e) {
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("databaseError", "" + databaseError.toString());
            }
        });

        //jezeli nie zalogowany, wyloguj
        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(getActivity(), WelcomeActivity.class));
        }


        return view;
    }


    private void loadRecyclerViewData(String mojKierownik) {
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kierownicy").child(mojKierownik).child("ListaMoichKurierow");
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                kurierModelList = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    KurierzyKontaktModel value = dataSnapshot1.getValue(KurierzyKontaktModel.class);
                    String valueKey = dataSnapshot1.getKey();
                    KurierzyKontaktModel kurierzyKontaktModel = new KurierzyKontaktModel();

                    Log.e("valueKey", "" + valueKey);
                    kurierzyKontaktModel.setEmail(value.getEmail());
                    kurierzyKontaktModel.setPhoneNumber(value.getPhoneNumber());
                    kurierzyKontaktModel.setUuid(valueKey);

                    kurierModelList.add(kurierzyKontaktModel);

                    kurierzyKontaktDataAdapter = new KurierzyKontaktDataAdapter(getActivity(), kurierModelList);
                    rvListaKurierowMojegoKierownika.setAdapter(kurierzyKontaktDataAdapter);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", "" + databaseError.getMessage());
            }
        });

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
