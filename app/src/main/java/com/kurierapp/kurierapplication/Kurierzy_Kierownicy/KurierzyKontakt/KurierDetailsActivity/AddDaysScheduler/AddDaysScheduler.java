package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.AddDaysScheduler;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierGrafikModel;
import com.kurierapp.kurierapplication.R;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddDaysScheduler extends Activity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private Unbinder unbind;

    @BindView(R.id.etDate)
    EditText etDate;

    @BindView(R.id.etTime)
    EditText etTime;

    @BindView(R.id.etPriorytet)
    EditText etPriorytet;

    @BindView(R.id.etNazwa)
    EditText etNazwa;

    @BindView(R.id.etOpis)
    EditText etOpis;

    @BindView(R.id.etMiasto)
    EditText etMiasto;

    @BindView(R.id.etUlica)
    EditText etUlica;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mReference;
    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_days_scheduler);
        unbind = ButterKnife.bind(this);

    }

    @OnClick(R.id.bSaveNewSchedule)
    public void onSaveNewScheduleButtonClick() {
        String mData = etDate.getText().toString();
        if (mData.isEmpty()) {
            //etDate.setError("Wybierz dzień");
            Toast.makeText(this, "Wybierz dzień", Toast.LENGTH_SHORT).show();
            return;
        }

        String mGodzina = etTime.getText().toString();
        if (mGodzina.isEmpty()) {
            //etTime.setError("Wybierz godzine");
            Toast.makeText(this, "Wybierz godzine", Toast.LENGTH_SHORT).show();

            return;
        }

        String mPriorytet = etPriorytet.getText().toString();
        if (mPriorytet.isEmpty()) {
            etPriorytet.setError("Nadaj priorytet");

            return;
        }

        String mNazwa = etNazwa.getText().toString();
        if (mNazwa.isEmpty()) {
            etNazwa.setError("Podaj nazwe");

            return;
        }

        String mOpis = etOpis.getText().toString();
        if (mOpis.isEmpty()) {
            mOpis = "Brak opisu";
        }

        String mMiasto = etMiasto.getText().toString();
        if (mMiasto.isEmpty()) {
            etMiasto.setError("Podaj miasto");

            return;
        }

        String mUlica = etUlica.getText().toString();
        if (mUlica.isEmpty()) {
            etUlica.setError("Podaj ulice");

            return;
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mUserId = mFirebaseUser.getUid();
        mReference = FirebaseDatabase.getInstance().getReference();

        try {
            Bundle extras = getIntent().getExtras();
            Log.e("odebralem uuid", "" + extras.getString("kurierUuid"));
            KurierGrafikModel kurierGrafikModel = new KurierGrafikModel(mPriorytet,
                    mData, mGodzina, mNazwa, mOpis, mMiasto, mUlica, extras.getString("kurierUuid"), "", "Do wykonania");

            mReference.child("Uzytkownicy")
                    .child("Kurierzy")
                    .child(extras.getString("kurierUuid"))
                    .child("Grafik")
                    .push()
                    .setValue(kurierGrafikModel);

            Toast.makeText(this, "Dodano.", Toast.LENGTH_SHORT).show();
            onBackPressed();

        } catch (Exception e) {
            Toast.makeText(this, "Błąd", Toast.LENGTH_SHORT).show();
            Log.e("error pushing item", "" + e.toString());
        }
    }

    @OnClick(R.id.etDate)
    public void onDatePickButtonClick() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        int miesiac = today.month + 1;

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this, today.year, miesiac, today.monthDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.etTime)
    public void onTimePickButtonClick() {
        Date currentTime = Calendar.getInstance().getTime();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, currentTime.getHours(), currentTime.getMinutes(), true);
        timePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        etDate.setText(day + "/" + month + "/" + year);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        etTime.setText(hour + ":" + minute);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
