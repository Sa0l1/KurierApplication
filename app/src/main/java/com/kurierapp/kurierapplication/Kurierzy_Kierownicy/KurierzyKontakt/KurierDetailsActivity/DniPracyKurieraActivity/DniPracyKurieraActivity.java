package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.DniPracyKurieraActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierDniPracyModel;
import com.kurierapp.kurierapplication.R;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DniPracyKurieraActivity extends Activity implements DatePickerDialog.OnDateSetListener {//, TimePickerDialog.OnTimeSetListener {

    @BindView(R.id.etDate)
    EditText etDate;

    @BindView(R.id.etStartTime)
    EditText etStartTime;

    @BindView(R.id.etEndTime)
    EditText etEndTime;

    private Unbinder unbind;

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dni_pracy_kuriera);
        unbind = ButterKnife.bind(this);
    }

    @OnClick(R.id.etDate)
    public void onDatePickButtonClick() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        int miesiac = today.month + 1;

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this, today.year, miesiac, today.monthDay);
        datePickerDialog.show();
    }

    /*@OnClick(R.id.etStartTime)
    public void onTimePickButtonClick() {
        Date currentTime = Calendar.getInstance().getTime();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, currentTime.getHours(), currentTime.getMinutes(), true);
        timePickerDialog.show();
    }

    @OnClick(R.id.etEndTime)
    public void onEndTimePickButtonClick() {
        Date currentTime = Calendar.getInstance().getTime();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, currentTime.getHours(), currentTime.getMinutes(), true);
        timePickerDialog.show();
    }*/

    @OnClick(R.id.bDodaj)
    public void onDodajButtonClick() {
        String mData = etDate.getText().toString();
        if (mData.isEmpty()) {
            Toast.makeText(this, "Wybierz dzień", Toast.LENGTH_SHORT).show();
            return;
        }

        String mGodzinaRozpoczecia = etStartTime.getText().toString();
        if (mGodzinaRozpoczecia.isEmpty()) {
            Toast.makeText(this, "Wybierz godzine rozpoczęcia", Toast.LENGTH_SHORT).show();

            return;
        }

        String mGodzinaZakonczenia = etEndTime.getText().toString();
        if (mGodzinaZakonczenia.isEmpty()) {
            Toast.makeText(this, "Wybierz godzine zakończenia", Toast.LENGTH_SHORT).show();

            return;
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mUserId = mFirebaseUser.getUid();
        mReference = FirebaseDatabase.getInstance().getReference();
        try {
            Bundle extras = getIntent().getExtras();
            Log.e("extras", "odebralem: " + extras.getString("kurierUuid"));

            KurierDniPracyModel kurierDniPracyModel = new KurierDniPracyModel(mData, mGodzinaRozpoczecia, mGodzinaZakonczenia);

            mReference.child("Uzytkownicy")
                    .child("Kurierzy")
                    .child(extras.getString("kurierUuid"))
                    .child("DniPracy")
                    .push()
                    .setValue(kurierDniPracyModel);

            Toast.makeText(this, "Dodano.", Toast.LENGTH_SHORT).show();
            onBackPressed();
            //view.openMealsFragment();

        } catch (Exception e) {
            Toast.makeText(this, "Błąd", Toast.LENGTH_SHORT).show();
            Log.e("error pushing item", "" + e.toString());
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        etDate.setText(day + "/" + month + "/" + year);
    }

    /*@Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        etStartTime.setText(hour + ":" + minute);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }
}