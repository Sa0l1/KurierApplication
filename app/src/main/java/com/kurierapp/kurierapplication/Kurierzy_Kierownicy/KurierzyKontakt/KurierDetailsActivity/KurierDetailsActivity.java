package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kierownik.KierownikMojaListaKurierowFragment.ListaKurierowModel;
import com.kurierapp.kurierapplication.Kurier.KurierMojProfilFragment.KurierDniPracyDataAdapter;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.AddDaysScheduler.AddDaysScheduler;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.DniPracyKurieraActivity.DniPracyKurieraActivity;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.SingleScheduleActivity.SingleScheduleActivity;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierzyKontaktModel;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KurierDetailsActivity extends Activity {

    @BindView(R.id.tvKurierEmail)
    TextView tvKurierEmail;

    @BindView(R.id.tvKurierPhone)
    TextView tvKurierPhone;

    @BindView(R.id.bAddToMyList)
    Button bAddToMyList;

    @BindView(R.id.bAddDaysScheduler)
    Button bAddDaysScheduler;

    @BindView(R.id.rvZadaniaKuriera)
    ListView rvZadaniaKuriera;

    @BindView(R.id.rvMojeDniPracy)
    RecyclerView rvMojeDniPracy;

    @BindView(R.id.bAddDniPracy)
    Button bAddDniPracy;

    @BindView(R.id.bFindLocation)
    Button bFindLocation;

    private EditText etDate;
    private EditText etStartTime;

    private KurierzyScheduleDataAdapter kurierzyScheduleDataAdapter;

    private static final String TAG = "KurierDetailsActivity";

    //firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;
    private DatabaseReference mReferenceToDelete;

    private String mUserId;

    private String emailAdresat;
    private List<KurierGrafikModel> kurierScheduleList;
    private List<ListaKurierowModel> kurierModelList;
    private List<KurierDniPracyModel> kurierDniPracyModelList;

    private Marker mUserMarker;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kurier_details);
        ButterKnife.bind(this);

        //wyswietlamy email i tel kuriera + sprawdzamy czy ma kuriera na liscie
        Bundle extras = getIntent().getExtras();
        emailAdresat = extras.getString("kurierEmail");
        tvKurierEmail.setText(extras.getString("kurierEmail"));
        tvKurierPhone.setText(extras.getString("kurierPhone"));
        Log.e("odebralem", "" + extras.getString("kurierUuid"));

        if (extras.getString("id") != null) {
            bAddToMyList.setVisibility(View.GONE);
            bAddDaysScheduler.setVisibility(View.VISIBLE);
            bAddDniPracy.setVisibility(View.VISIBLE);
            bFindLocation.setVisibility(View.VISIBLE);
        }
        //sprawdzam czy kurier czy kierownik
        SharedPreferences sharedPreferencesUserLogin = getSharedPreferences("ktoZalogowany", Context.MODE_PRIVATE);
        String ktoZalogowany = sharedPreferencesUserLogin.getString("ktoZalogowany", "");
        if (ktoZalogowany.equals("kurier")) {
            bAddToMyList.setVisibility(View.GONE);

        }

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(extras.getString("kurierUuid")).child("Grafik");

        //jezeli nie zalogowany, wyloguj
        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(this, WelcomeActivity.class));
        }

        //lista dni pracy
        rvMojeDniPracy.setHasFixedSize(true);
        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        rvMojeDniPracy.setLayoutManager(llm2);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);

        loadRecyclerViewDataListaZadan();
        loadRecyclerViewDataListaDniPracy();
    }

    @OnClick(R.id.textZadania)
    public void loadRecyclerViewDataListaZadan() {
        rvZadaniaKuriera.setVisibility(View.VISIBLE);
        rvMojeDniPracy.setVisibility(View.GONE);
        rvZadaniaKuriera.invalidate();
        final Bundle extras = getIntent().getExtras();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1);
        rvZadaniaKuriera.setAdapter(adapter);

        mReference = mFirebaseDatabase.getReference("Uzytkownicy");
        final String kurierUuid = extras.getString("kurierUuid");
        Log.e("kurierUuid", "" + kurierUuid);
        mReference.child("Kurierzy").child(kurierUuid).child("Grafik").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.add((String) dataSnapshot.child("nazwa").getValue());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.remove((String) dataSnapshot.child("nazwa").getValue());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        rvZadaniaKuriera.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                AlertDialog.Builder opcjeDialog = new AlertDialog.Builder(KurierDetailsActivity.this);
                opcjeDialog.setTitle("Wybierz");

                opcjeDialog.setPositiveButton("Wyswietl", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(kurierUuid).child("Grafik");
                        mReference.orderByChild("nazwa")
                                .equalTo((String) rvZadaniaKuriera.getItemAtPosition(position))
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Intent intent = new Intent(KurierDetailsActivity.this, SingleScheduleActivity.class);
                                        intent.putExtra("schedulePriorytet", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getPriorytet());
                                        intent.putExtra("scheduleNazwa", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getNazwa());
                                        intent.putExtra("scheduleOpis", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getOpis());
                                        intent.putExtra("scheduleDzien", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getDzien());
                                        intent.putExtra("scheduleGodzina", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getGodzina());
                                        intent.putExtra("scheduleMiasto", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getMiasto());
                                        intent.putExtra("scheduleUlica", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getUlica());
                                        intent.putExtra("scheduleNazwa", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getNazwa());
                                        intent.putExtra("scheduleStatus", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getStatus());
                                        intent.putExtra("valueKey", dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getKey());
                                        Log.e(TAG, "onDataChange: value key: " + dataSnapshot.getKey());
                                        Log.e(TAG, "onDataChange: status: " + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getStatus());
                                        Log.e("kurierzySchedule", "wysylam: " + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getUuid());
                                        intent.putExtra("kurierUuid", "" + dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getUuid());
                                        intent.putExtra("kurier", "kurier");

                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("error", "" + databaseError.getMessage());
                                    }
                                });
                    }
                }).setNegativeButton("Zakończ zadanie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(kurierUuid).child("Grafik");
                        mReference.orderByChild("nazwa")
                                .equalTo((String) rvZadaniaKuriera.getItemAtPosition(position))
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot dataSnapshot) {
                                        AlertDialog.Builder zakonczDialog = new AlertDialog.Builder(KurierDetailsActivity.this);
                                        zakonczDialog.setTitle("Zakończyć zadanie?");
                                        zakonczDialog.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (dataSnapshot.hasChildren()) {
                                                    String kurierUuid = dataSnapshot.getChildren().iterator().next().getValue(KurierGrafikModel.class).getUuid();
                                                    Log.e(TAG, "onDataChange: kurierUuid: " + kurierUuid);
                                                    try {
                                                        String valueKey = dataSnapshot.getChildren().iterator().next().getKey();
                                                        Log.e(TAG, "onDataChange: value key: " + valueKey);
                                                        mReference
                                                                .child(valueKey)
                                                                .child("status")
                                                                .setValue("Zakończono");
                                                        Toast.makeText(KurierDetailsActivity.this, "Zakończono zadanie", Toast.LENGTH_SHORT).show();
                                                        dialogInterface.dismiss();
                                                    } catch (Exception e) {
                                                    }
                                                }
                                            }
                                        }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });

                                        zakonczDialog.create().show();

                                        //usuwanie
                                        /*if (dataSnapshot.hasChildren()) {
                                            DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                                            firstChild.getRef().removeValue();
                                            Log.e("firstchild", "" + firstChild);
                                            Toast.makeText(KurierDetailsActivity.this, R.string.zakonczono, Toast.LENGTH_SHORT).show();
                                        }*/
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("onCancelled databaseErr", "" + databaseError);
                                    }
                                });
                    }
                });
                opcjeDialog.create().show();
            }
        });
    }

    @OnClick(R.id.textDniPracy)
    public void loadRecyclerViewDataListaDniPracy() {
        rvZadaniaKuriera.setVisibility(View.GONE);
        rvMojeDniPracy.setVisibility(View.VISIBLE);
        rvZadaniaKuriera.invalidate();
        Bundle extras = getIntent().getExtras();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(extras.getString("kurierUuid")).child("DniPracy");
        mReference.orderByChild("godzinaKoniec").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                kurierDniPracyModelList = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    KurierDniPracyModel value = dataSnapshot1.getValue(KurierDniPracyModel.class);
                    KurierDniPracyModel kurierDniPracyModel = new KurierDniPracyModel();

                    kurierDniPracyModel.setDzien(value.getDzien());
                    kurierDniPracyModel.setGodzinaStart(value.getGodzinaStart());
                    kurierDniPracyModel.setGodzinaKoniec(value.getGodzinaKoniec());

                    kurierDniPracyModelList.add(kurierDniPracyModel);

                    KurierDniPracyDataAdapter kurierDniPracyDataAdapter = new KurierDniPracyDataAdapter(getApplicationContext(), kurierDniPracyModelList);
                    rvMojeDniPracy.setAdapter(kurierDniPracyDataAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", "" + databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.bCallKurier)
    public void onCallKurierButtonClick() {
        Bundle extras = getIntent().getExtras();
        String kurierPhone = extras.getString("kurierPhone");

        Intent intentCallKurier = new Intent(Intent.ACTION_CALL);
        String kurierPhoneNumber = "tel:" + kurierPhone;
        intentCallKurier.setData(Uri.parse(kurierPhoneNumber));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intentCallKurier);
    }

    @OnClick(R.id.bSendSms)
    public void onSendMessageToKurierButtonClick() {
        Bundle extras = getIntent().getExtras();
        final String kurierPhone = extras.getString("kurierPhone");

        LayoutInflater inflater = LayoutInflater.from(this);
        View send_message_layout = inflater.inflate(R.layout.dialog_send_message, null);

        android.app.AlertDialog.Builder smsDialog = new android.app.AlertDialog.Builder(this);
        smsDialog.setTitle("Wyślij sms");

        final EditText etSendMessageContent = send_message_layout.findViewById(R.id.etSendMessageContent);

        smsDialog.setView(send_message_layout);
        smsDialog.setPositiveButton("Wyślij", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(kurierPhone, null, etSendMessageContent.getText().toString(), null, null);
            }
        }).setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        smsDialog.create().show();
    }

    @OnClick(R.id.bSendEmail)
    public void onSendEmailToKurierButtonClick() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View send_message_layout = inflater.inflate(R.layout.dialog_send_message, null);

        android.app.AlertDialog.Builder smsDialog = new android.app.AlertDialog.Builder(this);
        smsDialog.setTitle("Wyślij email");

        final EditText etSendMessageContent = send_message_layout.findViewById(R.id.etSendMessageContent);

        smsDialog.setView(send_message_layout);
        smsDialog.setPositiveButton("Wyślij", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                Bundle extras = getIntent().getExtras();
                emailAdresat = extras.getString("kurierEmail");
                Log.e("emailAdresat", "" + emailAdresat);

                String mEmailSubectMailFrom = FirebaseAuth.getInstance().getCurrentUser().getEmail();

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"" + emailAdresat});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mEmailSubectMailFrom);
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, etSendMessageContent.getText().toString());
                emailIntent.setType("message/rfc822");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Wybierz aplikacje"));
                } catch (Exception e) {
                    Log.e("sendEmail", "error: " + e.toString());
                }
            }
        }).setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        smsDialog.create().show();
    }

    @OnClick(R.id.bAddToMyList)
    public void onAddToMyListButtonClick() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mUserId = mFirebaseUser.getUid();
        mReference = FirebaseDatabase.getInstance().getReference();

        Bundle extras = getIntent().getExtras();
        final KurierzyKontaktModel kurierzyKontaktModel = new KurierzyKontaktModel(
                "" + extras.getString("kurierEmail"),
                "" + mUserId,
                "" + extras.getString("kurierPhone"),
                "" + mFirebaseUser.getEmail(),
                "" + extras.getString("kurierUuid")
        );

        try {
            mReference.child("Uzytkownicy")
                    .child("Kurierzy")
                    .child(extras.getString("kurierUuid"))
                    .child("Powiadomienia")
                    .push()
                    .setValue(kurierzyKontaktModel);

            onBackPressed();
            Toast.makeText(this, "Wysłano powiadomienie.", Toast.LENGTH_SHORT).show();
            //view.openMealsFragment();
        } catch (Exception e) {
            Toast.makeText(this, "Błąd", Toast.LENGTH_SHORT).show();
            Log.e("error pushing item", "" + e.toString());
        }

    }

    @OnClick(R.id.bAddDaysScheduler)
    public void onAddDaysSchedulerButtonClick() {
        Bundle extras = getIntent().getExtras();
        String kurierUuid = extras.getString("kurierUuid");
        Log.e("kurierDetails ode uuid", "" + kurierUuid);

        Intent intent = new Intent(KurierDetailsActivity.this, AddDaysScheduler.class);
        intent.putExtra("kurierUuid", kurierUuid);
        Log.e("wyslalem uuid", "" + kurierUuid);
        startActivity(intent);
    }

    @OnClick(R.id.bAddDniPracy)
    public void onAddDniPracyButtonClick() {
        Bundle extras = getIntent().getExtras();
        String kurierUuid = extras.getString("kurierUuid");

        Intent intent = new Intent(KurierDetailsActivity.this, DniPracyKurieraActivity.class);
        intent.putExtra("kurierUuid", kurierUuid);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //mini mapka z aktualna lokalizacja kuriera
    @OnClick(R.id.bFindLocation)
    public void onFindLocationButtonClick() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_map);

        MapsInitializer.initialize(this);

        MapView mMapView = dialog.findViewById(R.id.mapView);
        mMapView.onCreate(dialog.onSaveInstanceState());
        mMapView.onResume();// needed to get the map to display immediately
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                Bundle extras = getIntent().getExtras();
                DatabaseReference mKurierLocation = FirebaseDatabase.getInstance().getReference("AvaibleKuriers")
                        .child(extras.getString("kurierUuid")).child("l");

                Log.e("kurieruuid", "" + extras.getString("kurierUuid"));
                final ArrayList<String> myArrayList = new ArrayList<>();
                mKurierLocation.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.e("snapshot", "" + dataSnapshot.getChildren());
                        Log.e("snapshot", "" + dataSnapshot.toString());
                        Log.e("snapshot", "" + dataSnapshot.getValue());

                        if (dataSnapshot.hasChildren()) {
                            Double jeden = Double.valueOf(dataSnapshot.child("0").getValue().toString());
                            Double dwa = Double.valueOf(dataSnapshot.child("1").getValue().toString());
                            Log.e("0", "" + jeden);//.getSzerokosc()), value.getWysokosc());
                            Log.e("1", "" + dwa);//.getSzerokosc()), value.getWysokosc());

                            if (mUserMarker != null) {
                                mUserMarker.remove();
                            }

                            LatLng pozycja = new LatLng(jeden, dwa);
                            mUserMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(pozycja));

                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(pozycja));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        dialog.show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}