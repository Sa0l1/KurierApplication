package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity;

/**
 * Created by krzysztofm on 12.02.2018.
 */

public class KurierDniPracyModel {
    private String dzien;
    private String godzinaStart;
    private String godzinaKoniec;

    public KurierDniPracyModel() {
    }

    public KurierDniPracyModel(String dzien, String godzinaStart, String godzinaKoniec) {
        this.dzien = dzien;
        this.godzinaStart = godzinaStart;
        this.godzinaKoniec = godzinaKoniec;
    }

    public String getDzien() {
        return dzien;
    }

    public void setDzien(String dzien) {
        this.dzien = dzien;
    }

    public String getGodzinaStart() {
        return godzinaStart;
    }

    public void setGodzinaStart(String godzinaStart) {
        this.godzinaStart = godzinaStart;
    }

    public String getGodzinaKoniec() {
        return godzinaKoniec;
    }

    public void setGodzinaKoniec(String godzinaKoniec) {
        this.godzinaKoniec = godzinaKoniec;
    }
}
