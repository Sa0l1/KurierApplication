package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity;

/**
 * Created by staszekalcatraz on 04/02/2018.
 */

public class KurierGrafikModel {
    private String priorytet;
    private String dzien;
    private String godzina;
    private String nazwa;
    private String opis;
    private String miasto;
    private String ulica;
    private String uuid;
    private String key;
    private String status;
    private String valuekey;

    public KurierGrafikModel() {
    }

    public KurierGrafikModel(String priorytet, String dzien, String godzina, String nazwa, String opis, String miasto, String ulica, String uuid, String key, String status) {
        this.priorytet = priorytet;
        this.dzien = dzien;
        this.godzina = godzina;
        this.nazwa = nazwa;
        this.opis = opis;
        this.miasto = miasto;
        this.ulica = ulica;
        this.uuid = uuid;
        this.key = key;
        this.status = status;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(String priorytet) {
        this.priorytet = priorytet;
    }

    public String getDzien() {
        return dzien;
    }

    public void setDzien(String dzien) {
        this.dzien = dzien;
    }

    public String getGodzina() {
        return godzina;
    }

    public void setGodzina(String godzina) {
        this.godzina = godzina;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

