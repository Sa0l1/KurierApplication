package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.SingleScheduleActivity.SingleScheduleActivity;
import com.kurierapp.kurierapplication.R;

import java.util.List;

/**
 * Created by krzysztofm on 05.02.2018.
 */

public class KurierzyScheduleDataAdapter extends RecyclerView.Adapter<KurierzyScheduleDataAdapter.ViewHolder> {

    private static final String TAG = "KurierzyScheduleDataAda";

    private Context context;
    private List<KurierGrafikModel> kurierGrafikModelList;

    public KurierzyScheduleDataAdapter(Context context, List<KurierGrafikModel> kurierGrafikModels) {
        this.context = context;
        this.kurierGrafikModelList = kurierGrafikModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.kurier_schedule_row, parent, false);
        KurierzyScheduleDataAdapter.ViewHolder viewHolder = new KurierzyScheduleDataAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(KurierzyScheduleDataAdapter.ViewHolder holder, int position) {
        final KurierGrafikModel kurierGrafikModel = kurierGrafikModelList.get(position);
        holder.tvNazwa.setText(kurierGrafikModel.getNazwa());
        holder.tvStatus.setText(kurierGrafikModel.getStatus());
        holder.tvDzien.setText(kurierGrafikModel.getDzien());
        holder.tvGodzina.setText(kurierGrafikModel.getGodzina());
        holder.tvMiasto.setText(kurierGrafikModel.getMiasto());
        holder.tvUlica.setText(kurierGrafikModel.getUlica());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleScheduleActivity.class);
                intent.putExtra("schedulePriorytet", "" + kurierGrafikModel.getPriorytet());
                intent.putExtra("scheduleNazwa", "" + kurierGrafikModel.getNazwa());
                intent.putExtra("scheduleStatus", "" + kurierGrafikModel.getStatus());
                intent.putExtra("scheduleOpis", "" + kurierGrafikModel.getOpis());
                intent.putExtra("scheduleDzien", "" + kurierGrafikModel.getDzien());
                intent.putExtra("scheduleGodzina", "" + kurierGrafikModel.getGodzina());
                intent.putExtra("scheduleMiasto", "" + kurierGrafikModel.getMiasto());
                intent.putExtra("scheduleUlica", "" + kurierGrafikModel.getUlica());
                intent.putExtra("scheduleNazwa", "" + kurierGrafikModel.getNazwa());
                Log.e("kurierzySchedule", "wysylam uuid: " + kurierGrafikModel.getUuid());
                intent.putExtra("kurierUuid", "" + kurierGrafikModel.getUuid());
                intent.putExtra("valueKey", "" + kurierGrafikModel.getKey());
                Log.e(TAG, "onClick: " + kurierGrafikModel.getKey());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return kurierGrafikModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNazwa;
        private TextView tvDzien;
        private TextView tvGodzina;
        private TextView tvMiasto;
        private TextView tvUlica;
        private TextView tvStatus;

        public ViewHolder(View itemView) {
            super(itemView);

            tvNazwa = itemView.findViewById(R.id.tvNazwa);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvDzien = itemView.findViewById(R.id.tvDzien);
            tvGodzina = itemView.findViewById(R.id.tvGodzina);
            tvMiasto = itemView.findViewById(R.id.tvMiasto);
            tvUlica = itemView.findViewById(R.id.tvUlica);
        }
    }
}
