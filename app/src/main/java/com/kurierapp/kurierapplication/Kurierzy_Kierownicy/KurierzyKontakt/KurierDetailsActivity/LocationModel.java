package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity;

import java.io.Serializable;

/**
 * Created by staszekalcatraz on 05/02/2018.
 */

public class LocationModel {
    private String wysokosc;
    private String szerokosc;

    public LocationModel() {
    }

    public LocationModel(String wysokosc, String szerokosc) {
        this.wysokosc = wysokosc;
        this.szerokosc = szerokosc;
    }

    public String getWysokosc() {
        return wysokosc;
    }

    public LocationModel setWysokosc(String wysokosc) {
        this.wysokosc = wysokosc;
        return this;
    }

    public String getSzerokosc() {
        return szerokosc;
    }

    public LocationModel setSzerokosc(String szerokosc) {
        this.szerokosc = szerokosc;
        return this;
    }
}
