package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.SingleScheduleActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierGrafikModel;
import com.kurierapp.kurierapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SingleScheduleActivity extends Activity {

    private static final String TAG = "SingleScheduleActivity";

    @BindView(R.id.tvPriorytet)
    TextView tvPriorytet;

    @BindView(R.id.tvNazwa)
    TextView tvNazwa;

    @BindView(R.id.tvOpis)
    TextView tvOpis;

    @BindView(R.id.tvDzien)
    TextView tvDzien;

    @BindView(R.id.tvGodzina)
    TextView tvGodzina;

    @BindView(R.id.tvMiasto)
    TextView tvMiasto;

    @BindView(R.id.tvUlica)
    TextView tvUlica;

    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.bZakonczZadanie)
    Button bZakonczZadanie;

    private Unbinder unbind;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private String mUserId;
    private DatabaseReference mReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_schedule);
        unbind = ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        Log.e("odebralemuuid", "" + extras.getString("kurierUuid"));

        if (extras.getString("kurier", "").equals("kurier")) {
            bZakonczZadanie.setVisibility(View.GONE);
        }

        tvPriorytet.setText(extras.getString("schedulePriorytet"));
        tvNazwa.setText(extras.getString("scheduleNazwa"));
        tvStatus.setText("Status: " + extras.getString("scheduleStatus"));
        tvOpis.setText(extras.getString("scheduleOpis"));
        tvDzien.setText(extras.getString("scheduleDzien"));
        tvGodzina.setText(extras.getString("scheduleGodzina"));
        tvMiasto.setText(extras.getString("scheduleMiasto"));
        tvUlica.setText("Ul. " + extras.getString("scheduleUlica"));
    }

    @OnClick(R.id.bZakonczZadanie)
    public void onZakonczZadanieButtonClick() {
        AlertDialog.Builder zakonczenieDialog = new AlertDialog.Builder(this);
        zakonczenieDialog.setTitle("Zakończyć zadanie?");
        zakonczenieDialog.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Bundle extras = getIntent().getExtras();

                // Initialize Firebase Auth
                mFirebaseAuth = FirebaseAuth.getInstance();
                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                mFirebaseDatabase = FirebaseDatabase.getInstance();
                mUserId = mFirebaseUser.getUid();
                mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy").child(extras.getString("kurierUuid")).child("Grafik");
                mReference.orderByChild("nazwa")
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Bundle extras = getIntent().getExtras();
                                if (dataSnapshot.hasChildren()) {
                                    String kej = dataSnapshot.getChildren().iterator().next().getKey();
                                    Log.e(TAG, "onDataChange: kej: " + kej);
                                    try {
                                        String valueKey = extras.getString("valueKey");
                                        Log.e(TAG, "onDataChange: value key: " + valueKey);
                                        mReference
                                                .child(valueKey)
                                                .child("status")
                                                .setValue("Zakończono");
                                        Toast.makeText(SingleScheduleActivity.this, "Zakończono", Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                    } catch (Exception e) {
                                    }
                                }

                                //usuwanie
                                        /*if (dataSnapshot.hasChildren()) {
                                            DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                                            firstChild.getRef().removeValue();
                                            Log.e("firstchild", "" + firstChild);
                                            Toast.makeText(KurierDetailsActivity.this, R.string.zakonczono, Toast.LENGTH_SHORT).show();
                    }*/
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("onCancelled databaseErr", "" + databaseError);
                            }
                        });
            }
        }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        zakonczenieDialog.create().show();;

    }

    @OnClick(R.id.bNawiguj)
    public void onNawigujButtonClick() {
        Bundle extras = getIntent().getExtras();

        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + extras.getString("scheduleUlica") + "," + extras.getString("scheduleMiasto"));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
