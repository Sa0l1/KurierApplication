package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt.KurierDetailsActivity.KurierDetailsActivity;
import com.kurierapp.kurierapplication.R;

import java.util.List;

/**
 * Created by staszekalcatraz on 29/01/2018.
 */

public class KurierzyKontaktDataAdapter extends RecyclerView.Adapter<KurierzyKontaktDataAdapter.ViewHolder> {

    private Context context;
    private List<KurierzyKontaktModel> kurierModelList;

    public KurierzyKontaktDataAdapter(Context context, List<KurierzyKontaktModel> kurierModelList) {
        this.context = context;
        this.kurierModelList = kurierModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.kurier_kontakty_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final KurierzyKontaktModel kurierModel = kurierModelList.get(position);
        final String kurierEmail = kurierModel.getEmail();
        holder.tvKurierEmail.setText(kurierEmail);
        holder.tvKurierTelefon.setText(kurierModel.getPhoneNumber());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), KurierDetailsActivity.class);
                intent.putExtra("kurierEmail", "" + kurierEmail);
                Log.e("wysylam maila", "" + kurierEmail);
                intent.putExtra("kurierPhone", "" + kurierModel.getPhoneNumber());
                intent.putExtra("kurierUuid", "" + kurierModel.getUuid());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return kurierModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvKurierEmail, tvKurierTelefon;

        public ViewHolder(View itemView) {
            super(itemView);

            tvKurierEmail = itemView.findViewById(R.id.tvKurierEmail);
            tvKurierTelefon = itemView.findViewById(R.id.tvKurierTelefon);
        }
    }
}
