package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.WelcomeActivity;

import java.util.ArrayList;
import java.util.List;


public class KurierzyKontaktFragment extends Fragment {

    private RecyclerView rvListaKurierowKontakt;
    private List<KurierzyKontaktModel> kurierModelList;
    private KurierzyKontaktDataAdapter kurierzyKontaktDataAdapter;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mReference;

    public KurierzyKontaktFragment() {
        // Required empty public constructor
    }

    public static KurierzyKontaktFragment newInstance() {
        KurierzyKontaktFragment fragment = new KurierzyKontaktFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kurierzy_kontakt, container, false);
        rvListaKurierowKontakt = view.findViewById(R.id.rvListaKurierowKontakt);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mReference = mFirebaseDatabase.getReference("Uzytkownicy").child("Kurierzy");

        //jezeli nie zalogowany, wyloguj
        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(getActivity(), WelcomeActivity.class));
        }

        //lista
        rvListaKurierowKontakt.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvListaKurierowKontakt.setLayoutManager(llm);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        loadRecyclerViewData();

        return view;
    }

    private void loadRecyclerViewData() {
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                kurierModelList = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    KurierzyKontaktModel value = dataSnapshot1.getValue(KurierzyKontaktModel.class);
                    String valueKey = dataSnapshot1.getKey();
                    KurierzyKontaktModel kurierzyKontaktModel = new KurierzyKontaktModel();

                    Log.e("valueKey", "" + valueKey);
                    kurierzyKontaktModel.setEmail(value.getEmail());
                    kurierzyKontaktModel.setPhoneNumber(value.getPhoneNumber());
                    kurierzyKontaktModel.setUuid(valueKey);

                    kurierModelList.add(kurierzyKontaktModel);

                    kurierzyKontaktDataAdapter = new KurierzyKontaktDataAdapter(getActivity(), kurierModelList);
                    rvListaKurierowKontakt.setAdapter(kurierzyKontaktDataAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", "" + databaseError.getMessage());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
