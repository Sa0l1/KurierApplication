package com.kurierapp.kurierapplication.Kurierzy_Kierownicy.KurierzyKontakt;

import com.kurierapp.kurierapplication.WelcomeActivity.models.KurierModel;

/**
 * Created by krzysztofm on 01.02.2018.
 */

public class KurierzyKontaktModel {
    private String email;
    private String password;
    private String phoneNumber;
    private String typ;
    private String uuid = "";

    public KurierzyKontaktModel() {
    }

    public KurierzyKontaktModel(String email, String password, String phoneNumber, String typ, String uuid) {
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.typ = typ;
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public KurierzyKontaktModel setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }
}
