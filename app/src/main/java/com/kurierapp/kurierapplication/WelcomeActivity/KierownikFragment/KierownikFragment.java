package com.kurierapp.kurierapplication.WelcomeActivity.KierownikFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kurierapp.kurierapplication.Kierownik.KierownikMainActivity.KierownikMainActivity.KierownikMainActivity;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.models.KierownikModel;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;

public class KierownikFragment extends Fragment implements KierownikFragmentMVP.Presenter {

    private Button bKierownikLogin, bKierownikRegister;

    private Context context;
    private KierownikFragmentPresenter presenter;

    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private Unbinder unbinder;

    public KierownikFragment() {
        // Required empty public constructor
    }

    public static KierownikFragment newInstance() {
        KierownikFragment fragment = new KierownikFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kierownik, container, false);


        firebaseAuth = FirebaseAuth.getInstance();

        unbinder = ButterKnife.bind(view);

        bKierownikLogin = view.findViewById(R.id.bKierownikLogin);
        bKierownikLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onKierownikLoginButtonClicked();
            }
        });

        bKierownikRegister = view.findViewById(R.id.bKierownikRegister);
        bKierownikRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onKierownikRegisterClicked();
            }
        });

        return view;
    }

    //przycisk logowania kierownika
    //@OnClick(R.id.bKierownikLogin)
    public void onKierownikLoginButtonClicked() {
        final AlertDialog.Builder loginDialog = new AlertDialog.Builder(getActivity());
        loginDialog.setTitle("Logowanie");

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View login_layout = inflater.inflate(R.layout.dialog_kierownik_login, null);

        final EditText etEmail = login_layout.findViewById(R.id.etEmail);
        final EditText etPassword = login_layout.findViewById(R.id.etPassword);
        final AlertDialog dialogDots = new SpotsDialog(getActivity());

        //wyskakujace okienko
        loginDialog.setView(login_layout);
        loginDialog.setPositiveButton("Zaloguj kierownika", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialogInterface, int i) {
                String loginString = etEmail.getText().toString().trim();
                if (loginString.isEmpty()) {
                    Toast.makeText(getActivity(), "Wpisz email", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();
                    return;
                }

                final String passwordString = etPassword.getText().toString().trim();
                if (passwordString.length() <= 5) {
                    Toast.makeText(getActivity(), "Hasło jest za krótkie", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();

                    return;
                }

                dialogInterface.dismiss();
                dialogDots.show();

                firebaseAuth.signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                        .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                startActivity(new Intent(getActivity(), KierownikMainActivity.class));
                                dialogInterface.dismiss();
                                dialogDots.dismiss();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Błąd " + e, Toast.LENGTH_SHORT).show();
                                dialogDots.dismiss();
                            }
                        });

            }
        });
        loginDialog.setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        loginDialog.create().show();
    }

    //analogicznie do logowania, rejestracja
    public void onKierownikRegisterClicked() {
        final AlertDialog.Builder registerDialog = new AlertDialog.Builder(getActivity());
        registerDialog.setTitle("Rejestracja");

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View register_layout = inflater.inflate(R.layout.dialog_kierownik_register, null);

        final MaterialEditText etEmail = register_layout.findViewById(R.id.etEmail);
        final MaterialEditText etPassword = register_layout.findViewById(R.id.etPassword);
        final MaterialEditText etRepeatPassword = register_layout.findViewById(R.id.etRepeatPassword);
        final MaterialEditText etPhoneNumber = register_layout.findViewById(R.id.etPhoneNumber);
        final AlertDialog dialogDots = new SpotsDialog(getActivity());

        registerDialog.setView(register_layout);

        registerDialog.setPositiveButton("Zarejestruj kierownika", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, int i) {
                String emailString = etEmail.getText().toString();
                if (emailString.isEmpty()) {
                    Toast.makeText(getActivity(), "Wpisz email", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();
                    return;
                }

                final String passwordString = etPassword.getText().toString();
                if (passwordString.isEmpty()) {
                    Toast.makeText(getActivity(), "Hasło jest za krótkie", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();

                    return;
                }

                String repeatPasswordString = etRepeatPassword.getText().toString();
                if (!passwordString.equals(repeatPasswordString)) {
                    Toast.makeText(getActivity(), "Hasła się nie zgadzają", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();

                    return;
                }

                final String telefonString = etPhoneNumber.getText().toString();
                if (telefonString.isEmpty()) {
                    Toast.makeText(getActivity(), "Uzupełnij numer telefonu", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();

                    return;
                }
                if (telefonString.length() != 9) {
                    Toast.makeText(getActivity(), "Numer telefonu musi zawierać 9 liczb", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();

                    return;
                }

                if (!TextUtils.isDigitsOnly(telefonString)) {
                    Toast.makeText(getActivity(), "Podaj tylko cyfry", Toast.LENGTH_SHORT).show();
                    dialogDots.dismiss();

                    return;
                }

                firebaseAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                        .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                dialogInterface.dismiss();
                                databaseReference = FirebaseDatabase.getInstance().getReference();
                                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                                String mUserId = firebaseUser.getUid();
                                startActivity(new Intent(getActivity(), KierownikMainActivity.class));
                                dialogDots.dismiss();

                                //ustawienie modelu kierownika do pushniecia do bazy tak zeby byly widoczne te dane w bazie
                                KierownikModel kierownikModel = new KierownikModel(
                                        etEmail.getText().toString(),
                                        passwordString,
                                        etPhoneNumber.getText().toString(),
                                        "kierownik"
                                );

                                databaseReference
                                        .child("Uzytkownicy")
                                        .child("Kierownicy")
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .setValue(kierownikModel)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(getActivity(), "Zarejestrowano", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(getActivity(), "Błąd " + e, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Błąd " + e, Toast.LENGTH_SHORT).show();
                        dialogDots.dismiss();

                    }
                });
                dialogDots.show();

            }
        });

        registerDialog.setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        registerDialog.create().show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        unbinder.unbind();
    }
}
