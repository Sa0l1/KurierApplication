package com.kurierapp.kurierapplication.WelcomeActivity.KierownikFragment;

/**
 * Created by staszekalcatraz on 10/01/2018.
 */

interface KierownikFragmentMVP {

    interface View{}

    interface Presenter{}
}
