package com.kurierapp.kurierapplication.WelcomeActivity.KierownikFragment;

import android.content.Context;

/**
 * Created by staszekalcatraz on 10/01/2018.
 */

class KierownikFragmentPresenter implements KierownikFragmentMVP.Presenter{

    private KierownikFragmentMVP.View view;
    private Context context;

    public KierownikFragmentPresenter(KierownikFragmentMVP.View view, Context context) {
        this.view = view;
        this.context = context;
    }
}
