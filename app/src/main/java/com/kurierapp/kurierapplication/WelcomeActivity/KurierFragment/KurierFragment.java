package com.kurierapp.kurierapplication.WelcomeActivity.KurierFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kurierapp.kurierapplication.Kurier.KurierMainActivity.KurierMainActivity;
import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.models.KurierModel;
import com.rengwuxian.materialedittext.MaterialEditText;

import dmax.dialog.SpotsDialog;

public class KurierFragment extends Fragment implements KurierFragmentMVP.View {

    private Button bLoginKurier, bRegisterKurier;

    private KurierFragmentPresenter presenter;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private String mUserId;
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase;

    public KurierFragment() {
        // Required empty public constructor
    }

    public static KurierFragment newInstance() {
        KurierFragment fragment = new KurierFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    //fragment wyboru rejestracja/logowanie kuriera
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kurier, container, false);

        presenter = new KurierFragmentPresenter(this);
        final AlertDialog dialogDots = new SpotsDialog(getActivity());

        //po kliknieciu przycisku zaloguj
        bLoginKurier = view.findViewById(R.id.bKurierLogin);
        bLoginKurier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //presenter.onLoginButtonClicked();
                final AlertDialog.Builder loginDialog = new AlertDialog.Builder(getActivity());
                loginDialog.setTitle("Logowanie kuriera");

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View login_layout = inflater.inflate(R.layout.dialog_kurier_login, null);

                //pola email i haslo
                final MaterialEditText etEmail = login_layout.findViewById(R.id.etEmail);
                final MaterialEditText etPassword = login_layout.findViewById(R.id.etPassword);

                //wyskakujace okienko
                loginDialog.setView(login_layout);
                loginDialog.setPositiveButton("Zaloguj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String loginString = etEmail.getText().toString().trim();
                        if (loginString.isEmpty()) {
                            Toast.makeText(getActivity(), "Wpisz email", Toast.LENGTH_SHORT).show();
                            dialogDots.dismiss();
                            return;
                        }

                        final String passwordString = etPassword.getText().toString().trim();
                        if (passwordString.length() <= 5) {
                            Toast.makeText(getActivity(), "Hasło jest za krótkie", Toast.LENGTH_SHORT).show();
                            dialogDots.dismiss();

                            return;
                        }

                        dialogInterface.dismiss();
                        dialogDots.show();

                        firebaseDatabase = FirebaseDatabase.getInstance();
                        firebaseAuth = FirebaseAuth.getInstance();
                        //wyslanie zapytania do firebase z podanymi danymi
                        firebaseAuth.signInWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        openKurierMainActivity();
                                        dialogDots.dismiss();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        dialogDots.dismiss();
                                        showToastMessage("Błąd " + e);
                                        //Toast.makeText(context, "Błąd " + e, Toast.LENGTH_SHORT).show();
                                    }
                                });

                    }
                });
                loginDialog.setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                loginDialog.create().show();
            }
        });

        //analogicznie do logowania
        bRegisterKurier = view.findViewById(R.id.bKurierRegister);
        bRegisterKurier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //presenter.onRegisterButtonClicked();
                final AlertDialog.Builder registerDialog = new AlertDialog.Builder(getActivity());
                registerDialog.setTitle("Rejestracja kuriera");

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View register_layout = inflater.inflate(R.layout.dialog_kurier_register, null);

                final MaterialEditText etEmail = register_layout.findViewById(R.id.etEmail);
                final MaterialEditText etPassword = register_layout.findViewById(R.id.etPassword);
                final MaterialEditText etRepeatPassword = register_layout.findViewById(R.id.etRepeatPassword);
                final MaterialEditText etPhoneNumber = register_layout.findViewById(R.id.etPhoneNumber);

                registerDialog.setView(register_layout);

                registerDialog.setPositiveButton("Zarejestruj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, int i) {
                        String emailTextString = etEmail.getText().toString();
                        if (emailTextString.isEmpty()) {
                            Toast.makeText(getActivity(), "Wpisz email", Toast.LENGTH_SHORT).show();

                            return;
                        }

                        final String passwordString = etPassword.getText().toString();
                        if (passwordString.length() <= 5) {
                            Toast.makeText(getActivity(), "Hasło jest za krótkie", Toast.LENGTH_SHORT).show();


                            return;
                        }

                        String repeatPasswordString = etRepeatPassword.getText().toString();
                        if (!passwordString.equals(repeatPasswordString)) {
                            Toast.makeText(getActivity(), "Hasła nie są zgodne", Toast.LENGTH_SHORT).show();

                            return;
                        }

                        String phoneNumberString = etPhoneNumber.getText().toString();
                        if (phoneNumberString.isEmpty()) {
                            Toast.makeText(getActivity(), "Wpisz numer telefonu", Toast.LENGTH_SHORT).show();

                            return;
                        }

                        if (phoneNumberString.length() != 9) {
                            Toast.makeText(getActivity(), "Numer telefonu musi zawierać 9 liczb", Toast.LENGTH_SHORT).show();
                            dialogDots.dismiss();

                            return;
                        }

                        dialogDots.show();


                        firebaseAuth = FirebaseAuth.getInstance();
                        firebaseAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        databaseReference = FirebaseDatabase.getInstance().getReference();
                                        firebaseUser = firebaseAuth.getCurrentUser();
                                        mUserId = firebaseUser.getUid();
                                        openKurierMainActivity();
                                        dialogInterface.dismiss();
                                        dialogDots.dismiss();


                                        KurierModel kurierModel = new KurierModel(
                                                etEmail.getText().toString(),
                                                passwordString,
                                                etPhoneNumber.getText().toString(),
                                                "kurier");

                                        databaseReference
                                                .child("Uzytkownicy")
                                                .child("Kurierzy")
                                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                .setValue(kurierModel)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Toast.makeText(getActivity(), "Zarejestrowano", Toast.LENGTH_SHORT).show();
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Toast.makeText(getActivity(), "Błąd " + e, Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Błąd " + e, Toast.LENGTH_SHORT).show();
                                dialogInterface.dismiss();

                                dialogDots.dismiss();
                            }
                        });

                    }
                });

                registerDialog.setNegativeButton("Odrzuć", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                registerDialog.create().show();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void openKurierMainActivity() {
        startActivity(new Intent(getActivity(), KurierMainActivity.class));
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmailError(String message) {

    }

    @Override
    public void showPasswordError(String message) {

    }
}
