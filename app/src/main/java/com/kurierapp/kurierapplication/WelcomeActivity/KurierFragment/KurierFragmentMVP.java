package com.kurierapp.kurierapplication.WelcomeActivity.KurierFragment;

/**
 * Created by staszekalcatraz on 11/01/2018.
 */

public interface KurierFragmentMVP {

    interface View {
        void openKurierMainActivity();

        void showToastMessage(String message);

        void showEmailError(String message);

        void showPasswordError(String message);
    }

    interface Presenter {
        void onLoginButtonClicked();

        void onRegisterButtonClicked();
    }
}
