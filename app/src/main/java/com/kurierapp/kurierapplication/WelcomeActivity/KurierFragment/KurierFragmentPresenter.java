package com.kurierapp.kurierapplication.WelcomeActivity.KurierFragment;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by staszekalcatraz on 11/01/2018.
 */

class KurierFragmentPresenter implements KurierFragmentMVP.Presenter {

    private KurierFragmentMVP.View view;
    private Context context;
    private FirebaseAuth firebaseAuth;

    public KurierFragmentPresenter(KurierFragmentMVP.View view) {
        this.view = view;
    }

    @Override
    public void onLoginButtonClicked() {


    }

    @Override
    public void onRegisterButtonClicked() {

    }
}
