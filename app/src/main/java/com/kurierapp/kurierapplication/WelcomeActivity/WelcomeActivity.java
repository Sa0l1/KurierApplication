package com.kurierapp.kurierapplication.WelcomeActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.kurierapp.kurierapplication.R;
import com.kurierapp.kurierapplication.WelcomeActivity.KierownikFragment.KierownikFragment;
import com.kurierapp.kurierapplication.WelcomeActivity.KurierFragment.KurierFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WelcomeActivity extends AppCompatActivity implements WelcomeActivityMVP.Presenter {

    private Fragment fragment;

    private Unbinder unbind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        unbind = ButterKnife.bind(this);
    }

    @OnClick(R.id.bKierownik)
    public void onKierownikButtonClicked() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentFrameLayout, KierownikFragment.newInstance());
        fragmentTransaction.commit();
    }

    @OnClick(R.id.bKurier)
    public void onKurierButtonClicked() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentFrameLayout, KurierFragment.newInstance());
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
    }
}
