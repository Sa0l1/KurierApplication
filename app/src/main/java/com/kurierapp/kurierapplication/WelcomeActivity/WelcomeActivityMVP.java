package com.kurierapp.kurierapplication.WelcomeActivity;

/**
 * Created by staszekalcatraz on 10/01/2018.
 */

interface WelcomeActivityMVP {
    interface View{}

    interface Presenter{
        void onKierownikButtonClicked();

        void onKurierButtonClicked();
    }
}
