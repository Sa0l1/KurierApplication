package com.kurierapp.kurierapplication.WelcomeActivity.models;

/**
 * Created by staszekalcatraz on 10/01/2018.
 */

public class KierownikModel {
    private String email;
    private String password;
    private String typ;
    private String phoneNumber;

    public KierownikModel(String email, String password, String phoneNumber, String typ) {
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.typ = typ;
    }

    public String getEmail() {
        return email;
    }

    public KierownikModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public KierownikModel setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getTyp() {
        return typ;
    }

    public KierownikModel setTyp(String typ) {
        this.typ = typ;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public KierownikModel setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
