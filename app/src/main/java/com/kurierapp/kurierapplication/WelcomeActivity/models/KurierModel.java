package com.kurierapp.kurierapplication.WelcomeActivity.models;

/**
 * Created by staszekalcatraz on 11/01/2018.
 */

public class KurierModel {
    private String email;
    private String password;
    private String phoneNumber;
    private String typ;

    public KurierModel() {
    }

    public KurierModel(String email, String password, String phoneNumber, String typ) {
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.typ = typ;
    }

    public String getEmail() {
        return email;
    }

    public KurierModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public KurierModel setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public KurierModel setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getTyp() {
        return typ;
    }

    public KurierModel setTyp(String typ) {
        this.typ = typ;
        return this;
    }
}
